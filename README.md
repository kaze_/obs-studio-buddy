### Download

[download](https://gitlab.com/kaze_/obs-studio-buddy/-/releases) 

### Build

```
# with npm
npm run release

# with yarn
yarn release
```

### What does obs-studio-buddy do?

`obs-studio-buddy` needs to be running along side **obs-studio** and **Streamer.bot**. When running, `obs-studio-buddy` creates a server and waits for specific UDP messages from **Streamer.bot** to update death counts.

The name of the current game is determined by checking the current scene collection name on **obs-studio**.

If the current scene collection name is "`League of Legends`"  files related to it, like the `deaths.txt` will be stored in the `C:\Users\<user>\AppData\Roaming\obs-studio-buddy\league_of_legends` directory.

> **Note**: The game directory name will be in snake case.

### Requirements

[obs-studio](https://github.com/obsproject/obs-studio/releases/)

[obs-websocket](https://github.com/obsproject/obs-websocket) (is an **obs-studio** plugin) (version [5.0.0-beta1](https://github.com/obsproject/obs-websocket/releases/tag/5.0.0-beta1) is used, **version 4 will not work**)

[Streamer.bot](https://streamer.bot/)

### Instructions

Open **obs-websocket** settings from **obs-studio** main interface (```Tools => obs-websocket Settings```), enable **obs-websocket** server, enable authentication and set password to ```QBc0JymnvuYghmmh```

> if you want to set a different password, go ahead, but you will have to update ```C:\Users\<user>\AppData\Roaming\obs-studio-buddy\obs-websocket-password.txt``` with the password you set.

Open **Streamer.bot** and import the action script given below. This will create two actions,  ```death_counter +1``` and ```death_counter -1```.

```
TlM0RR+LCAAAAAAABADdUstugzAQvFfqPyCuyVYGGwy99dR7ryWqFntJIhFDeTSNIv69GBQF8rj10p5sz+yO1+M5Pj44jvtFVb0tjPvseMsBMLij/uRqwmbzoYrWNFS5I4eq6Wvrnn63Z8c5jktPbbVtSoXwMWYBEHoKBOcMohgRvMjnIkYWUcpGraHps6XWXmbaPD+jZDDNyeo1VUtn/OZkzsKbCK6roi3vTD8UYL7HQ/3W2gdnmNcT+QqNLnYvwxOvWVUY1VYVmeaau7JlZs1QUhaVbRRCBMs5gYe8wMG8YzIfO+nBxF0kbufOe0avSUqlFcVAQaxBkO9DrHkGnGPoxT1LOrto3NN2vbFzsCc2Z5pDab315+jJztn/jCMYTd9W6Ix2p+3q0plXKzPYs5oamudY1qQn7Eh2y9vh0pJnEWccSLAMBJKCVKYENlisz1uQcfrlcME/DxfcDRcL41BHEmQqyYYLASnjkIaBiphSMvK9vxkuu4yVY0JOZPcD3yKEBRAFAAA=
```

Next, link a chat command or keyboard shortcut to the ```death_counter +1``` and ```death_counter -1``` actions on **Streamer.bot**.

Start **obs-studio-buddy.exe** when **obs-studio** and **Streamer.bot** are running.

All done.

