import dgram from 'dgram'
import 'dotenv/config'
import fs from 'fs'
import { replace, snakeCase, startsWith, toInteger } from 'lodash'
import OBSWebSocket, { OBSWebSocketError } from 'obs-websocket-js'

const obs_studio_buddy_directory = `${process.env.APPDATA}\\obs-studio-buddy`
const obs_ws_port = getOBSWebsocketPort(obs_studio_buddy_directory) > 0
  ? getOBSWebsocketPort(obs_studio_buddy_directory)
  : setOBSWebsocketPort(obs_studio_buddy_directory, toInteger(process.env.DEFAULT_OBS_WEBSOCKET_PORT)) // default port is 4444
const obs_ws_password = getOBSWebsocketPassword(obs_studio_buddy_directory).length > 0
  ? getOBSWebsocketPassword(obs_studio_buddy_directory)
  : setOBSWebsocketPassword(obs_studio_buddy_directory, process.env.DEFAULT_OBS_WEBSOCKET_PASSWORD) // a default password

const server = dgram.createSocket('udp4')
const obs = new OBSWebSocket()

if (!fs.existsSync(obs_studio_buddy_directory)) {
  fs.mkdirSync(obs_studio_buddy_directory)
}

async function main() {

  await obs.connect(`ws://127.0.0.1:${obs_ws_port}`, obs_ws_password)

  server.on('error', (err) => {
    console.log(`server error:\n${err.stack}`)
    server.close()
    process.exit()
  })

  server.on('listening', () => {
    const address = server.address()
    console.log(`server listening ${address.address}:${address.port}`)
  })

  server.on('message', async (msg, rinfo) => {

    let message: {
      death_counter: "+" | "-"
    } = JSON.parse(msg.toString())
    let current_game: string = await getCurrentGame()

    if (current_game.length == 0) {
      console.error("ERROR: could not indentify current game.")
      console.error("POSSIBLE SOLUTION: make sure current scene name is in the format \"GAME <game name>\", eg. \"GAME League of Legends\"")
    }

    if (current_game.length > 0) {

      console.log({
        current_game: current_game,
        recieved_message: message
      })

      // death counter
      if (message.death_counter && message.death_counter.length > 0) {
        if (message.death_counter == "+") deathCounter(obs_studio_buddy_directory, current_game, "+")
        if (message.death_counter == "-") deathCounter(obs_studio_buddy_directory, current_game, "-")
      }

    }

  })

  server.bind(4445)
  // Prints: server listening 0.0.0.0:4445
}

(async () => {
  try {

    await main()
    console.log("Everything seems to be working as expected!")

  }
  catch (error: any) {

    if (error instanceof OBSWebSocketError) {
      if (error.code == 4009) {
        console.log("ERROR: ", error.message)
        console.log("POSSIBLE SOLUTIONS: ")
        console.log(`    either set the password used by obs-websocket to "${obs_ws_password}"`)
        console.log(`    or update ${obs_studio_buddy_directory}\\obs-websocket-password.txt to the password set on obs-websocket`)
      }
      if (error.code == -1) {
        console.log("ERROR: ", error.message)
        console.log("POSSIBLE SOLUTIONS: ")
        console.log("    1. make sure obs-studio is running")
        console.log("    2. make sure Streamer.bot is running")
        console.log("    3. make sure obs-websocket (the plugin) is installed")
        console.log(`    4. make sure obs-websocket is using port ${obs_ws_port}.`)
        console.log(`       for using a different port, update ${obs_studio_buddy_directory}\\obs-websocket-port.txt`)
        console.log(`    5. make sure obs-websocket needs "${obs_ws_password}" as password`)
        console.log(`       for using a different password, update ${obs_studio_buddy_directory}\\obs-websocket-password.txt`)
      }
    }

    console.log(" ")
    console.log('Press any key to exit')
    process.stdin.setRawMode(true)
    process.stdin.resume()
    process.stdin.on('data', process.exit.bind(process, 0))
  }

})()


async function getCurrentGame(): Promise<string> {
  let current_scene_collection_name = (await obs.call("GetSceneCollectionList")).currentSceneCollectionName
  return current_scene_collection_name
}


async function wait(ms: number) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(ms)
    }, ms)
  })
}


async function deathCounter(obs_studio_buddy_directory: string, game_name: string, action: "+" | "-") {

  let file_directory = `${obs_studio_buddy_directory}\\${snakeCase(game_name)}`
  let file_name = "deaths.txt"
  let file = `${file_directory}/${file_name}`

  if (game_name.length == 0) {
    console.log("can't update death counter, unknown game.")
    return
  }

  if (fs.existsSync(file_directory) == false) {
    fs.mkdirSync(file_directory, { recursive: true })
  }

  let deaths: number = 0
  if (fs.existsSync(file)) {
    deaths = action == "+"
      ? toInteger(fs.readFileSync(file, "utf-8")) + 1
      : toInteger(fs.readFileSync(file, "utf-8")) - 1
    writeTextToFile(file_directory, file_name, deaths.toString())
  } else {
    deaths = 1
    writeTextToFile(file_directory, file_name, deaths.toString())
  }

  console.log({ game: game_name, deaths: deaths })

}


function writeTextToFile(directory: string, file_name: string, data: string) {

  if (fs.existsSync(directory) == false) fs.mkdirSync(directory, { recursive: true })
  let file_path = `${directory}\\${file_name}`
  fs.writeFileSync(file_path, data, "utf8")

}


function getOBSWebsocketPassword(obs_studio_buddy_directory: string): string {

  let file_directory = obs_studio_buddy_directory
  let file_name = "obs-websocket-password.txt"
  let file = `${file_directory}/${file_name}`

  let password = ""

  if (fs.existsSync(file) == true) {
    password = fs.readFileSync(file, "utf-8")
  }

  return password

}


function setOBSWebsocketPassword(obs_studio_buddy_directory: string, password: string): string {

  let file_directory = obs_studio_buddy_directory
  let file_name = "obs-websocket-password.txt"
  let file = `${file_directory}/${file_name}`

  writeTextToFile(file_directory, file_name, password)

  return password

}


function getOBSWebsocketPort(obs_studio_buddy_directory: string): number {

  let file_directory = obs_studio_buddy_directory
  let file_name = "obs-websocket-port.txt"
  let file = `${file_directory}/${file_name}`

  let port = -1

  if (fs.existsSync(file) == true) {
    port = toInteger(fs.readFileSync(file, "utf-8"))
  }

  return port

}


function setOBSWebsocketPort(obs_studio_buddy_directory: string, port: number): number {

  let file_directory = obs_studio_buddy_directory
  let file_name = "obs-websocket-port.txt"
  let file = `${file_directory}/${file_name}`

  writeTextToFile(file_directory, file_name, port.toString())

  return port

}